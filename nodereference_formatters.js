Drupal.behaviors.nodereference_formatters = function (context) {
  $('a.nodereference-cluetip').cluetip({
    width: '700px', 
    showTitle: false,
    sticky: true, 
    activation: 'hover', 
    attribute: 'href',  // DO NOT change this line unless you know what you are doing.
    mouseOutClose: true, 
    closePosition: 'bottom', 
    closeText: 'X', 
    fx: { 
      open: 'slideDown', 
      openSpeed: '1000'
    },
    onShow: function() { tb_init('a.thickbox') },
  });
}